# demo-1

This is a simple demographics data study project and the main aim of this is to create a 'everything at a single place kind of source' for the data. This data could be later used in different applications like education, statistics, etc.

Following are the current highest priority tasks:
- find structure to demographics data, independent of the origin (country)
- find official sources to the data to update regularly
- decide the applications this data will be used and iterate to find all the data required for the application - for example education :
    - are we making an e-learning application for students to understand the geography, polity, sociology of a region?
    - are we making a statistical tool which can take other data and return result? for example, a virulant disease spreads in your area, find the high risk zones?
- depending on the application the relevant area for data will change, but there must be something that connects it all?
